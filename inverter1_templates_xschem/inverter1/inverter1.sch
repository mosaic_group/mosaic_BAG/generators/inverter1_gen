v {xschem version=3.1.0 file_version=1.2 }
G {}
K {}
V {}
S {}
E {}
N 0 -200 0 -110 {
lab=Q}
N -0 -150 100 -150 {
lab=Q}
N -60 -230 -40 -230 {
lab=I}
N -60 -230 -60 -80 {
lab=I}
N -60 -80 -40 -80 {
lab=I}
N -70 -150 -60 -150 {
lab=I}
N -110 -150 -70 -150 {
lab=I}
N -110 -290 0 -290 {
lab=VDD}
N 0 -290 -0 -260 {
lab=VDD}
N -110 -0 -0 -0 {
lab=VSS}
N -0 -50 -0 -0 {
lab=VSS}
N -0 -230 10 -230 {
lab=VDD}
N 10 -290 10 -230 {
lab=VDD}
N 0 -290 10 -290 {
lab=VDD}
N 0 -80 10 -80 {
lab=VSS}
N 10 -80 10 0 {
lab=VSS}
N 0 0 10 0 {
lab=VSS}
C {BAG_prim/pmos4_standard/pmos4_standard.sym} -20 -230 0 0 {name=Xpmos
l=lch
w=500n
model=pmos4_standard
}
C {devices/ipin.sym} -110 -150 0 0 {name=p1 lab=I}
C {devices/opin.sym} 100 -150 0 0 {name=p2 lab=Q}
C {devices/iopin.sym} -110 -290 0 1 {name=p3 lab=VDD}
C {devices/iopin.sym} -110 0 0 1 {name=p4 lab=VSS}
C {BAG_prim/nmos4_standard/nmos4_standard.sym} -20 -80 0 0 {name=Xnmos
l=lch
w=500n
model=nmos4_standard
}
C {BAG_prim/nmos4_standard/nmos4_standard.sym} 230 -80 0 0 {name=Xdummy
l=lch
w=500n
model=nmos4_standard
}
C {devices/lab_pin.sym} 250 -110 2 0 {name=l1 sig_type=std_logic lab=VSS}
C {devices/lab_pin.sym} 210 -80 0 0 {name=l2 sig_type=std_logic lab=VSS}
C {devices/lab_pin.sym} 250 -80 2 0 {name=l3 sig_type=std_logic lab=VSS}
C {devices/lab_pin.sym} 250 -50 0 0 {name=l4 sig_type=std_logic lab=VSS}
