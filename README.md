# Inverter
This project is structured example of how to generate an inverter with mosaic_BAG

Supposed to be run in MOSAIC BAG virtuoso_template structure

Howto:
1) git submodule add this module to your virtuoso project  installation directory
2) cd inverter gen 
3) Run './configure'
4) Run 'make gen' This template default to finfet process
   Possible variants:
   'make gen flip\_well' for flip well processes
   'make gen finfet' for flip well processes
   'make gen cmos' for standard palnar cmos
   'make gen BUILDOPTS='--finfet --flip_well'' or any combination

4) Schematic and layout are generated to default_lib_path; usually "${BAG_WORK_DIR}/gen_libs" 

Metheds for LVS and DRC are implemented in configure, and in active use in Aalto University. 
Do not remove. API for checks needs to be designed.

